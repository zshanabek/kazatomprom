class AddOrganizationIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :organization, foreign_key: true, null: true
    add_column :users, :block_ids, :string, array: true, default: []
  end
end
