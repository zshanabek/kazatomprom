class CreateFlowmeters < ActiveRecord::Migration[5.1]
  def change
    create_table :flowmeters do |t|
      t.string :name
      t.integer :volume
      t.integer :compare
      t.integer :u
      t.string :ur_flowmeter
      t.references :block, index: true, foreign_key:{ to_table: :blocks}
      t.timestamps
    end
  end
end
