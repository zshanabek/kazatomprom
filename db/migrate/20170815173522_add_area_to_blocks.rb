class AddAreaToBlocks < ActiveRecord::Migration[5.1]
  def change
    add_reference :blocks, :area, foreign_key: true, index: true
  end
end
