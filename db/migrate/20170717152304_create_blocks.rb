class CreateBlocks < ActiveRecord::Migration[5.1]
  def change
    create_table :blocks do |t|
      t.string :name
      t.string :area_name
      t.references :organization, index: true, foreign_key: { to_table: :organizations}
      # t.references :area, index: true, foreign_key: { to_table: :areas}      
      t.timestamps
    end
  end
end
