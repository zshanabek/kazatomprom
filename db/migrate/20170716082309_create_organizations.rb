class CreateOrganizations < ActiveRecord::Migration[5.1]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :address
      t.string :slug
      t.references :owner, index: true, foreign_key: { to_table: :users }
      t.timestamps
    end
    add_index :organizations, :slug, unique: true
  end
end
