class CreateAreas < ActiveRecord::Migration[5.1]
  def change
    create_table :areas do |t|
      t.string :name
      t.references  :parent
      t.references :organization, index: true, foreign_key: { to_table: :organizations}
      t.timestamps
    end
  end
end
