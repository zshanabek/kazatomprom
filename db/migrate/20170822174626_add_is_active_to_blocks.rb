class AddIsActiveToBlocks < ActiveRecord::Migration[5.1]
  def change
    add_column :blocks, :is_active, :boolean    
  end
end
