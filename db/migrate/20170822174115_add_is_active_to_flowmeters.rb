class AddIsActiveToFlowmeters < ActiveRecord::Migration[5.1]
  def change
    add_column :flowmeters, :is_active, :boolean
  end
end
