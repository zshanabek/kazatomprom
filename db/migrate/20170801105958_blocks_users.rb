class BlocksUsers < ActiveRecord::Migration[5.1]
  def change
     create_table :blocks_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :block, index: true
    end
  end
end
