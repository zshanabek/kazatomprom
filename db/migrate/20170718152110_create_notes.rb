class CreateNotes < ActiveRecord::Migration[5.1]
  def change
    create_table :notes do |t|
      t.float :volume_value
      t.integer :pr
      t.integer :vr,          null: true                   
      t.string :shift
      t.integer :block_name
      t.integer :block_id
      t.string :flowmeter_name
      t.boolean :is_sent
      t.references :flowmeter, index: true, foreign_key:{to_table: :flowmeters}
      t.references :user, index: true, foreign_key:{to_table: :users} 
      t.timestamps
    end
  end
end
