# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170822174626) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.bigint "parent_id"
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_areas_on_organization_id"
    t.index ["parent_id"], name: "index_areas_on_parent_id"
  end

  create_table "blocks", force: :cascade do |t|
    t.string "name"
    t.string "area_name"
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "area_id"
    t.boolean "is_active"
    t.index ["area_id"], name: "index_blocks_on_area_id"
    t.index ["organization_id"], name: "index_blocks_on_organization_id"
  end

  create_table "blocks_users", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "block_id"
    t.index ["block_id"], name: "index_blocks_users_on_block_id"
    t.index ["user_id"], name: "index_blocks_users_on_user_id"
  end

  create_table "flowmeters", force: :cascade do |t|
    t.string "name"
    t.integer "volume"
    t.integer "compare"
    t.integer "u"
    t.string "ur_flowmeter"
    t.bigint "block_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active"
    t.index ["block_id"], name: "index_flowmeters_on_block_id"
  end

  create_table "notes", force: :cascade do |t|
    t.float "volume_value"
    t.integer "pr"
    t.integer "vr"
    t.string "shift"
    t.integer "block_name"
    t.integer "block_id"
    t.string "flowmeter_name"
    t.boolean "is_sent"
    t.bigint "flowmeter_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["flowmeter_id"], name: "index_notes_on_flowmeter_id"
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "slug"
    t.bigint "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_organizations_on_owner_id"
    t.index ["slug"], name: "index_organizations_on_slug", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.boolean "is_admin", default: false, null: false
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token", limit: 30
    t.bigint "organization_id"
    t.string "block_ids", default: [], array: true
    t.boolean "is_logged_in", default: false
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["organization_id"], name: "index_users_on_organization_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "areas", "organizations"
  add_foreign_key "blocks", "areas"
  add_foreign_key "blocks", "organizations"
  add_foreign_key "flowmeters", "blocks"
  add_foreign_key "notes", "flowmeters"
  add_foreign_key "notes", "users"
  add_foreign_key "organizations", "users", column: "owner_id"
  add_foreign_key "users", "organizations"
end
