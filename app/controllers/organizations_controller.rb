# frozen_string_literal: true
class OrganizationsController < ApplicationController
    before_action :set_organization, only: [:show, :update, :destroy]

    def index
        @organizations = Organization.all
        render json: @organizations, status: :ok
    end
    
    def show
        render json: @organization, status: :ok   
    end

    def create
      # if user_signed_in?
      #   if current_user.is_admin?
          organization = Organization.create(organization_params)
          # organization.owner_id = current_user.id
          if organization.save!
            render json: organization, status: :created
          else
            head(:unprocessable_entity)
          end
      #   else
      #     head(:unauthorized)
      #   end
      # else
      #   head(:forbidden)
      # end
    end

    def update
      # if user_signed_in?
      #   if current_user.is_admin?      
          if @organization.update(organization_params)
            render json: @organization
          else
            head(:unprocessable_entity)
          end
      #   else
      #     head(:unauthorized)
      #   end
      # else
      #   head(:forbidden)
      # end
    end

    def destroy
      # if user_signed_in?
      #   if current_user.is_admin?            
          if @organization.destroy
              head (:ok)
          else
              head (:unprocessable_entity)
          end
      #   else
      #     head(:unauthorized)
      #   end
      # else
      #   head(:forbidden)
      # end
  end

    private
    
    def set_organization
      @organization = Organization.friendly.find(params[:id])
    end

    def organization_params
      params.require(:organization).permit(:name, :slug, :address, :owner_id)
    end

    
end
