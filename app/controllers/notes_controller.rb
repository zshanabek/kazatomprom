# frozen_string_literal: true
require 'nokogiri'
class NotesController < ApplicationController
  include Notes::Response
  before_action :set_note, only: [:show, :update, :destroy]

  def index
    @notes = current_flowmeter.notes     
    render json: @notes, :include => {:user => {:only => :username}} 
  end
  
  def multi_create
    success = Note.multi_create(request.raw_post)
    if success
      render json: {success: 'Показания успешно отправлены'}, status: :created
    else
      render json: {fail: 'Показания не отправились'}, status: :unprocessable_entity
    end
  end

  def all_notes_xml
    # if user_signed_in?
    #   if current_user.is_admin?
        @filename = current_organization.name  
        @notes = current_organization.notes   
        @result = @notes.joins(:flowmeter).group(['blocks.name','flowmeters.name', 'notes.created_at' ]).sum(:volume_value)
        @dates = @result.map {|(block_name, flowmeter_name,  date), volume_value | date }.uniq.sort      

        respond_to do |format|
          format.xml {response.headers['Content-Disposition'] = "attachment; filename='#{@filename} - показания.xml'"}      
        end  
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #     head(:forbidden)
    # end
  end
  
  def all_notes_axlsx
    date_filter = params[:date_filter].to_i
    if date_filter!=0
      @notes = current_organization.notes.where(created_at: date_filter.days.ago..Time.current)
    else
      @notes = current_organization.notes.all
    end  
    @result = @notes.joins(:flowmeter).group(['blocks.name','flowmeters.name', 'notes.created_at' ]).sum(:volume_value)
    @dates = @result.map {|(block_name, flowmeter_name,  date), volume_value | date }.uniq.sort 
    @filename = current_organization.name
    respond_to do |format|
      format.xlsx { response.headers['Content-Disposition'] = "attachment; filename='#{@filename} - показания.xlsx'"}        
    end  
  end


  def all_notes
    # if user_signed_in?
    #   if current_user.is_admin?
    date_filter = params[:date_filter].to_i
    if date_filter!=0
      @notes = current_organization.notes.where(created_at: date_filter.days.ago..Time.current)
    else
      @notes = current_organization.notes.all
    end          
      
    # @notes = current_organization.notes.all        
    # render json: @notes, :include => {:user => {:only => :username}, :flowmeter => {:only => :id, :name}, :block => {:only => :id, :name}}, status: :ok    
    render :all_notes, status: :ok
        #   else
    #     head(:unauthorized)
    #   end
    # else
    #     head(:forbidden)
    # end
  end

  def show
    render json: @note, status: :ok
  end

  def create
    # if user_signed_in?
      note = current_flowmeter.notes.build(note_params)
      # note.user_id = current_user.id
      create_and_render_note(note) || render_invalid_response
    # else
    #   head(:forbidden)
    # end
  end

  def update
    update_and_render_note(@note, note_params) || render_invalid_response
  end

  def destroy
    if @note.destroy
      head(:ok)
    else
      head(:unprocessable_entity)
    end
  end

  private

  def current_organization
    @current_organization ||= Organization.friendly.find(params[:organization_id])
  end

  def current_block
    @current_block ||=  current_organization.blocks.find(params[:block_id])
  end

  def current_flowmeter
    @current_flowmeter ||=  current_block.flowmeters.find(params[:flowmeter_id])
  end

  def note_params
    params.require(:note).permit(:volume_value,:is_sent, :pr, :vr, :shift, :block_name, :block_id, :flowmeter_name, :flowmeter_id, :user_id, :created_at, :updated_at)
  end

  def set_note
      @note = current_flowmeter.notes.find(params[:id])
  end
  

end
