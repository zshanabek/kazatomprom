class DepositsController < ApplicationController
    before_action :set_deposit, only: [:show, :update, :destroy]
    
    def index
        @area = current_organization.areas.find(params[:area_id])
        @deposits = @area.deposits
        render json: @deposits, status: :ok
    end
    
    def show
        render json: @deposit, status: :ok   
    end

    def create
        # if user_signed_in?
        #   if current_user.is_admin?
            @deposit = current_organization.areas.create(deposit_params)
            @deposit.parent_id = current_area.id
            if @deposit.save!
                render json: @deposit, status: :created
            else
                head(:unprocessable_entity)
            end
        #   else
        #     head(:unauthorized)
        #   end
        # else
        #   head(:forbidden)
        # end
    end

    def update
        # if user_signed_in?
        #   if current_user.is_admin?      
            if @deposit.update(deposit_params)
                render json: @deposit
            else
                head(:unprocessable_entity)
            end
        #   else
        #     head(:unauthorized)
        #   end
        # else
        #   head(:forbidden)
        # end
    end

    def destroy
        # if user_signed_in?
        #   if current_user.is_admin?            
            if @deposit.destroy
                head (:ok)
            else
                head (:unprocessable_entity)
            end
        #   else
        #     head(:unauthorized)
        #   end
        # else
        #   head(:forbidden)
        # end
    end

    private

        def current_organization
            @current_organization ||= Organization.friendly.find(params[:organization_id])
        end

        def current_area
            @area = current_organization.areas.find(params[:area_id])
        end
        
        def set_deposit
            @deposit = current_area.deposits.find(params[:id])
        end

        def deposit_params
            params.require(:deposit).permit(:name, :parent_id, :organization_id)
        end
        
end
