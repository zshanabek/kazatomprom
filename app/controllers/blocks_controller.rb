# frozen_string_literal: true
class BlocksController < ApplicationController
  before_action :set_block, only: [:show, :update, :destroy]
  def index
    @blocks = current_organization.blocks.all
    render :index, status: :ok
  end
  

  def areas_index
    @blocks = current_area.blocks.all
    render :areas_index, status: :ok
  end

  def deposits_index
    @blocks = current_deposit.blocks.all
    render :areas_index, status: :ok
  end

  def show
    render json: @block, status: :ok
  end

  def create
    # if user_signed_in?
    #   if current_user.is_admin?

        @block = current_organization.blocks.build(block_params)
        area_name = block_params["area_name"]
        area = Area.find_by name: area_name
        @block.organization_id = current_organization.id
        @block.area_id = area.id
        if @block.save!
          # render :create, status: :created, locals: {block: block}
          render json: @block, status: :created   
        else
          head(:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  def batch_update
    success = Block.batch_update(request.raw_post)
    if success
      render json: {success: 'Блоки успешно изменены'}, status: :ok
    else
      render json: {fail: 'Не удалось изменить блоки'}, status: :unprocessable_entity
    end
  end

  def update
    # if user_signed_in?
    #   if current_user.is_admin?
        if @block.update(block_params)
          head :no_content
        else
          head(:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  def destroy
    # if user_signed_in?
    #   if current_user.is_admin?
        if @block.destroy
            head (:ok)
        else
            head (:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  private
    def current_organization
      @current_organization ||= Organization.friendly.find(params[:organization_id])
    end

    def current_area
      @current_area = current_organization.areas.find(params[:area_id])
    end

    def current_deposit
      @current_deposit = current_area.deposits.find(params[:deposit_id])
    end

    def set_block
      @block = current_organization.blocks.find(params[:id])
    end

    def block_params
      params.require(:block).permit(:name, :organization_id, :area_name, :area_id, :is_active)
    end
    
end
