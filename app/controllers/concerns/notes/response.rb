# frozen_string_literal: true

module Notes
    # Response for contacts controller
    module Response
      extend ActiveSupport::Concern

      def update_and_render_note(note, params)
        note.update(params) &&
          render(json: note, locals: { note: note })
      end

      def create_and_render_note(note)
        note.save! &&
          render(json: note, status: :created, locals: { note: note })
      end

      def render_invalid_response
        head(:unprocessable_entity)
      end
    end
end
