# frozen_string_literal: true

class SessionsController<ApplicationController
    def show
        current_user ? head(:ok) :head(:unauthorized)
    end

    def create
        @user = User.where(username: params[:username]).first
        if @user&.valid_password?(params[:password])
            if @user.is_admin?
                render json: @user
            else
                @org_id = @user.organization_id
                @organization = Organization.find(@org_id)
                @blocks = @user.blocks.all    
                render :json => {
                    :user => @user.as_json(:except => [:block_id ] , :include => {:organization => {:only => :name}} ),
                    :blocks => @blocks.collect{ |i| i.as_json(:only => [:id, :name, :area_name]) }
                }   
            end 
        else
            head(:unauthorized)
        end
    end

    def destroy
        current_user&.authentication_token = nil
        if current_user&.save
            head(:ok)
        else
            head(:unauthorized)
        end
    end


    def current_organization
        @current_organization ||= Organization.friendly.find(params[:organization_id])
    end
end
