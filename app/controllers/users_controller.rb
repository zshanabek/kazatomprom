# frozen_string_literal: true
class UsersController < ApplicationController
    before_action :set_user, only: [:show, :update,  :destroy]
    def index
        # if current_user.is_admin?
            @users = current_organization.employees.all
            render :index, status: :ok
            
        # else
        #     head(:unauthorized)
        # end
    end

    def batch_create
        success = current_organization.employees.batch_create(request.raw_post)
        if success
            render json: {success: 'Пользователи успешно созданы'}, status: :created
        else
            render json: {fail: 'Не удалось создать пользователей'}, status: :unprocessable_entity
        end
    end
    
    def show
        @blocks = @user.blocks.all    
        render :show, status: :ok
    end

    def create
        # if user_signed_in?
        #     if current_user.is_admin?
                @user = current_organization.employees.build({:block_ids => params[:block_ids], :first_name => params[:first_name], :last_name => params[:last_name], :username => params[:username], :organization_id => params[:organization_id],  :password => params[:password], :password_confirmation => params[:password_confirmation] })
                @user.organization_id = current_organization.id
                @user.username = (@user.first_name[0] + @user.last_name).to_s.downcase
                if User.exists?(username: @user.username)
                    render json: {fail: 'Пользователь с таким именем уже существует'}, status: :created
                else
                    if @user.save!
                        render json: @user
                    else
                        head :unprocessable_entity
                    end
                end
        #     else
        #         head(:unauthorized)
        #     end
        # else
        #     head(:forbidden)
        # end
    end

    def update_password
        # if user_signed_in?
        #     if current_user.is_admin?
                @user = User.find(params[:user_id])
                if @user.update_attribute(:password, params[:password])
                    bypass_sign_in(@user)                    
                    head :no_content
                else
                    head(:unprocessable_entity)
                end
        #     else
        #         head(:unauthorized)
        #     end
        # else
        #     head(:forbidden)
        # end
    end

    def update
        # if user_signed_in?
        #     if current_user.is_admin?
                if @user.update(user_params)
                    head :no_content
                else
                    head(:unprocessable_entity)
                end
        #     else
        #         head(:unauthorized)
        #     end
        # else
        #     head(:forbidden)
        # end
    end

    def destroy
        # if user_signed_in?
        #     if current_user.is_admin?
                if @user.destroy
                    head :ok
                else
                    head :unprocessable_entity
                end
        #     else
        #         head(:unauthorized)
        #     end
        # else
        #     head(:forbidden)
        # end
    end

    private

    def user_params 
      params.require(:user).permit( :first_name, :last_name, :username, :password, :password_confirmation,:organization_id, :block_ids=>[])
    end

    def set_user
        @user = current_organization.employees.find(params[:id])
    end


    def current_organization
      @current_organization ||= Organization.friendly.find(params[:organization_id])
    end

end
