# frozen_string_literal: true

class FlowmetersController < ApplicationController
  before_action :set_flowmeter, only: [:show, :update, :destroy]
  def index
    @flowmeters = current_block.flowmeters.all
    render json: @flowmeters, :include => {:block => {:only => :name}}, status: :ok   
  end


  def organization_index
    @flowmeters = current_organization.flowmeters.all
    render json: @flowmeters, :include => {:block => {:only => :name}}, status: :ok    
  end

  def show
    render json: @flowmeter, status: :ok
  end

  def create
    # if user_signed_in?
    #   if current_user.is_admin?
        @flowmeter = current_block.flowmeters.build(flowmeter_params)
        if @flowmeter.save!
          # render :create, status: :created, locals: {flowmeter: flowmeter}
          render json: @flowmeter, status: :created    
        else
          head(:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  def batch_create
    success = current_block.flowmeters.batch_create(request.raw_post)
    if success
      render json: {success: 'Скважины успешно отправлены'}, status: :created
    else
      render json: {fail: 'Скважины не отправились'}, status: :unprocessable_entity
    end
  end
  
  def update
    # if user_signed_in?
    #   if current_user.is_admin?
        if @flowmeter.update(flowmeter_params)
          head :no_content
        else
          head(:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  def destroy
    # if user_signed_in?
    #   if current_user.is_admin?
        if @flowmeter.destroy
            head (:ok)
        else
            head (:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  private
    def current_organization
      @current_organization ||= Organization.friendly.find(params[:organization_id])
    end

    def current_area
      @current_area ||= current_organization.areas.find(params[:area_id])
    end

    def current_deposit
      @current_deposit ||= current_area.deposit.find(params[:area_id])
    end

    def current_block
        @current_block = current_organization.blocks.find(params[:block_id])
    end

    def set_flowmeter
      @flowmeter = current_organization.flowmeters.find(params[:id])
    end

    def set_area_flowmeter
      @flowmeter = current_area.flowmeters.find(params[:id])
    end

    def set_deposit_flowmeter
      @flowmeter = current_deposit.flowmeters.find(params[:id])
    end

    def flowmeter_params
      params.require(:flowmeter).permit(:name, :volume, :compare, :u, :ur_flowmeter, :block_id, :is_active)
    end


end
