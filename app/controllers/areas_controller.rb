class AreasController < ApplicationController
  before_action :set_area, only: [:show, :update, :destroy]

  def index 
    @areas = current_organization.areas.where(parent_id: nil)
    render json: @areas, status: :ok
  end

  def show
    # @areas = current_organization.areas.find(params[:id])
    # @areas = @area.deposits 
    render json: @area, status: :ok
  end

  def create
    # if user_signed_in?
    #   if current_user.is_admin?
        @area = current_organization.areas.build(area_params)
        @area.parent = current_organization.areas.find(params[:id]) unless params[:id].nil?
        if @area.save!
          # render :create, status: :created, locals: {area: area}
          render json: @area, status: :created, locals: {area: @area}      
        else
          head(:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  def update
    # if user_signed_in?
    #   if current_user.is_admin?
        if @area.update(area_params)
          head :no_content
        else
          head(:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  def destroy
    # if user_signed_in?
    #   if current_user.is_admin?
        if @area.destroy  
            head (:ok)
        else
            head (:unprocessable_entity)
        end
    #   else
    #     head(:unauthorized)
    #   end
    # else
    #   head(:forbidden)
    # end
  end

  private
    def current_organization
      @current_organization ||= Organization.friendly.find(params[:organization_id])
    end

    def set_area
      @area = current_organization.areas.find(params[:id])
    end

    def area_params
      params.require(:area).permit(:name, :organization_id, :parent_id)
    end

end
