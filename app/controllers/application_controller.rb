class ApplicationController < ActionController::API
    acts_as_token_authentication_handler_for User, fallback: :none  
    before_action :set_raven_context
    include ActionController::MimeResponds  
    include Response
    include ExceptionHandler

    before_action :configure_permitted_parameters, if: :devise_controller?

    def configure_permitted_parameters
        update_attrs = [:password, :password_confirmation, :current_password]
        devise_parameter_sanitizer.permit :account_update, keys: update_attrs
    end

    private
    
    def set_raven_context
        Raven.user_context(id: session[:current_user_id]) # or anything else in session
        Raven.extra_context(params: params.to_unsafe_h, url: request.url)
    end
end
