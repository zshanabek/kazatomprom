json.array! @users do |user|
    json.id user.id
    json.is_logged_in user.is_logged_in    
    json.first_name user.first_name
    json.last_name user.last_name    
    json.username user.username      
    json.is_admin user.is_admin  
    json.organization_id user.organization.id
    json.created_at user.created_at
    json.updated_at user.updated_at    
    json.block_ids user.block_ids    
    
    json.organization do
        json.id user.organization.id        
        json.name user.organization.name
    end
    @blocks = Block.where(id: user.block_ids)
    json.blocks @blocks.collect{ |i| i.as_json(:only => [:id,:name]) }
end