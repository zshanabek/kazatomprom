json.data do
    json.user do
        json.call(
            @user,
            :username,
            :authentication_token
        )
    end
end
