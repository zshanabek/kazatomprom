json.data do
    json.block do
        json.partial! 'blocks/block', block: block
    end
end