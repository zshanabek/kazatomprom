json.array! @blocks do |block|
    json.id block.id
    json.name block.name
    json.organization_id block.organization_id
    json.created_at block.created_at
    json.area_name block.area_name
    json.area_id block.area_id

    json.area do
        json.id block.area.id
        json.name block.area.name
    end
    
    json.organization do
        json.id block.organization.id        
        json.name block.organization.name
    end
end