json.array! @notes do |note|
    json.id note.id
    json.volume_value note.volume_value
    json.shift note.shift
    json.user_id note.user_id     
    json.flowmeter_id note.flowmeter_id
    json.block_id note.block_id
    json.created_at note.created_at
    json.updated_at note.updated_at     
    
    json.user do
        json.first_name note.user.first_name
        json.last_name note.user.last_name        
    end
    json.flowmeter do
        json.id note.flowmeter.id        
        json.name note.flowmeter.name
    end

    json.block do 
        json.id note.flowmeter.block.id        
        json.name note.flowmeter.block.name
    end
end