xml.instruct!
xml.notes do
  @notes.each do |note|
    xml.note do
      xml.block note.flowmeter.block.name            
      xml.flowmeter note.flowmeter.name      
      xml.volume_value note.volume_value
      xml.created_at note.created_at      
    end
  end
end