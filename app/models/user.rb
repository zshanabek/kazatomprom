# frozen_string_literal: true
class User < ApplicationRecord
  acts_as_token_authenticatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:username]
  has_many :organizations, foreign_key: 'owner_id'
  belongs_to :organization, optional: true
  has_many :notes, dependent: :destroy
  has_and_belongs_to_many :blocks
  
  def email_required? 
    false
  end

  def email_changed? 
    false 
  end
  def will_save_change_to_email?
    false
  end

  def self.batch_create(post_content)
    begin
        User.transaction do
            JSON.parse(post_content).each do |user_hash|
                User.create!(user_hash)
            end 
        end 
    rescue
        Rails.logger.info("Не созданы пользователи")
        false
    end  
  end
end
