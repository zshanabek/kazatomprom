class Block < ApplicationRecord
  has_many :flowmeters, dependent: :destroy
  has_many :notes, through: :flowmeters
  belongs_to :organization
  belongs_to :deposit, class_name: 'Area', optional: true
  belongs_to :area, class_name: 'Area', optional: true  
  has_and_belongs_to_many :users
end
