class Organization < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :areas
  has_many :blocks
  has_many :flowmeters, through: :blocks
  has_many :notes, through: :flowmeters
  belongs_to :owner, class_name: 'User'
  
  has_many :employees, class_name: "User", foreign_key: 'organization_id', dependent: :destroy

  def normalize_friendly_id(text)
    text.to_slug.transliterate(:russian).normalize.to_s
  end
end
