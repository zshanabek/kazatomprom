class Area < ApplicationRecord
    has_many :deposits, class_name: 'Area', foreign_key: 'parent_id'
    has_many :blocks
    belongs_to :organization
end
