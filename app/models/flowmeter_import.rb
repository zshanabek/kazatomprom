class FlowmeterImport
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :file

    def initialize(attributes = {})
        attributes.each{|name, value| send("#{name}=",value)}
    end

    def persisted
        false
    end

    def imported_flowmeters
        @imported_flowmeters ||= load_imported_flowmeters
    end

    def load_imported_flowmeters
        spreadsheet = open_spreadsheet
        header = spreadsheet.row(1)
        (2..spreadsheet.last_row).map do |i|
            row = Hash[[header, spreadsheet.row(i)].transpose]
            flowmeter = Flowmeter.find_by_id(row["id"]) || Flowmeter.new
            flowmeter.attributes = row.to_hash.slice(*Flowmeter.accessible_attributes)
            flowmeter
        end
    end

    def open_spreadsheet
        case File.extname(file.original_filename)
        when ".csv" then Csv.new(file.path, nil, :ignore)
        when ".xlsx" then Excelx.new(file.path, nil, :ignore)
        else raise "Unknown file type: #{file.original_filename}"
        end
    end
end