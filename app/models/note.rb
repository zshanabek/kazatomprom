# frozen_string_literal: true
class Note < ApplicationRecord
    belongs_to :flowmeter
    belongs_to :block, optional: :true
    belongs_to :user

    def self.multi_create(post_content)
        begin
            Note.transaction do
                JSON.parse(post_content).each do |note_hash|
                    next if unless note_hash["volume_value"].nil?
                        Note.create!(note_hash)
                    end
                end 
            end 
        rescue
            Rails.logger.info("Не созданы множественные показания")
            false   
        end  
    end  

end
