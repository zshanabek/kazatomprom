class Flowmeter < ActiveRecord::Base
    has_many :notes, dependent: :destroy
    belongs_to :block


    def self.batch_create(post_content)
        begin
            Flowmeter.transaction do
                JSON.parse(post_content).each do |flowmeter_hash|
                    Flowmeter.create!(flowmeter_hash)
                end 
            end 
        rescue
            Rails.logger.info("Не созданы множественные скважины")
            false
        end  
    end  

end
