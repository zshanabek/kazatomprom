# frozen_string_literal: true
Rails.application.routes.draw do
 
  devise_for :users

  resources :organizations, defaults: { format: :json } do
    resources :areas do
      resources :blocks, except: :index do
        resources :flowmeters do
          resources :notes 
        end
      end      
      resources :deposits do   
        resources :blocks, except: :index do
          resources :flowmeters do
            resources :notes 
          end
        end
      end
    end
    resources :users
    resources :notes
    resources :blocks, except: :index do
      resources :flowmeters do 
        resources :notes  
      end
    end
  end
  resource :sessions, only: %i[create destroy show]
  match 'organizations/:organization_id/blocks' => 'blocks#index', via: :get, defaults: { format: :json }    
  match 'organizations/:organization_id/areas/:area_id/blocks' => 'blocks#areas_index', via: :get, defaults: { format: :json }  
  match 'organizations/:organization_id/areas/:area_id/deposits/:deposit_id/blocks' => 'blocks#deposits_index', via: :get, defaults: { format: :json }    
  match 'organizations/:organization_id/all_notes.xlsx' => 'notes#all_notes_axlsx', via: :get 
  match 'organizations/:organization_id/all_notes_xml.xml' => 'notes#all_notes_xml', via: :get   
  match 'organizations/:organization_id/all_notes/' => 'notes#all_notes',  via: :get, defaults: { format: :json }
  match 'organizations/:organization_id/multi_create' => 'notes#multi_create', via: :post,  defaults: { format: :json }
  match 'organizations/:organization_id/flowmeters' => 'flowmeters#organization_index', via: :get ,  defaults: { format: :json }
  match 'users/:user_id' => 'users#update_password', via: [:put,:patch],  defaults: { format: :json }
  match 'organizations/:organization_id/blocks/:block_id/batch_create' => 'flowmeters#batch_create', via: :post,  defaults: { format: :json }
  match 'organizations/:organization_id/blocks/:block_id/batch_update' => 'flowmeters#batch_update', via: :put,  defaults: { format: :json }  
  match 'organizations/:organization_id/batch_update' => 'blocks#batch_update', via: :put,  defaults: { format: :json } 
  match 'organizations/:organization_id/users/batch_create' => 'users#batch_create', via: :post,  defaults: { format: :json }     
end
