require_relative 'boot'
require "csv"
require "rails/all"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

class Application < Rails::Application
  # Initialize configuration defaults for originally generated Rails version.
  config.load_defaults 5.1
  config.time_zone = 'Astana'
  config.i18n.default_locale = :ru
  config.encoding = "utf-8"
  config.filter_parameters << :password
  Raven.configure do |config|
    config.dsn = 'https://9cd52114cbd44d55b4e4c180ef116564:dd8eeec9cb15477fb8d4fdd7d492af11@sentry.io/204361'
  end

  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.

  # Only loads a smaller set of middleware suitable for API only apps.
  # Middleware like session, flash, cookies can be added back manually.
  # Skip views, helpers and assets when generating a new resource.
  config.api_only = true

  config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins '*'
      resource( 
        '*', 
        headers: :any, 
        methods: [:get, :patch, :put, :delete, :post, :options]
      )
    end
  end
end